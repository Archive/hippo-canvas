import pygtk
pygtk.require('2.0')
import gtk
import hippo

window = gtk.Window()

canvas = hippo.Canvas()
theme = hippo.CanvasTheme(theme_stylesheet="test.css")
canvas.set_theme(theme)

window.add(canvas)
canvas.show()

box = hippo.CanvasBox(xalign=hippo.ALIGNMENT_FILL,
		      yalign=hippo.ALIGNMENT_FILL,
                      orientation=hippo.ORIENTATION_VERTICAL)
canvas.set_root(box)
canvas.set_size_request(400, 400)

text = hippo.CanvasText(text="Default Text")
box.append(text)

box2 = hippo.CanvasBox(xalign=hippo.ALIGNMENT_FILL,
                       yalign=hippo.ALIGNMENT_FILL,
                       orientation=hippo.ORIENTATION_VERTICAL,
                       id="box2")
box.append(box2)
text = hippo.CanvasText(text="Larger Text")
box2.append(text)
text = hippo.CanvasText(text="Red Text", classes="important")
box2.append(text)

link = hippo.CanvasLink(text="A Link")
box2.append(link)

link = hippo.CanvasLink(text="Visited Link", visited=True)
box2.append(link)

parent = box
for i in xrange(0, 5):
    nested = hippo.CanvasBox(xalign=hippo.ALIGNMENT_FILL,
                             yalign=hippo.ALIGNMENT_FILL,
                             classes="nested")
    parent.append(nested)
    parent = nested
    
window.show()

gtk.main()
