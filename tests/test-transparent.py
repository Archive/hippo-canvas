#!/usr/bin/python
# coding=UTF_8

import cairo
import pygtk
pygtk.require('2.0')
import gtk
from expand_box import ExpandBox
import hippo
import sys

window = hippo.CanvasWindow()

if not window.get_screen().is_composited():
    print >>sys.stderr, "Compositing manager must be running"
    sys.exit(1)

colormap = window.get_screen().get_rgba_colormap()
if colormap == None:
    print >>sys.stderr, "RGBA visual not found"
    sys.exit(1)

window.set_colormap(colormap)
window.set_decorated(False)

theme = hippo.CanvasTheme(theme_stylesheet="test-transparent.css")
window.set_theme(theme)

window.connect('delete-event', lambda *args: gtk.main_quit())

root = hippo.CanvasBox(id='root')
window.set_root(root)

top_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL)
root.append(top_box)

header = hippo.CanvasBox(id='header')
top_box.append(header, hippo.PACK_EXPAND)

header_right = ExpandBox(id='header-right', yalign=hippo.ALIGNMENT_CENTER)
header_right.set_clickable(True)
top_box.append(header_right)

close_x = hippo.CanvasBox(id='close-x', box_width=10, box_height=10)
header_right.append(close_x)

header_text = hippo.CanvasText(text="Khayyam via Fitzgerald", xalign=hippo.ALIGNMENT_START)
header.append(header_text)

main = hippo.CanvasBox(id='main')
root.append(main, hippo.PACK_EXPAND)

expand = ExpandBox()
main.append(expand, hippo.PACK_EXPAND)

text = u"""
Think, in this batter’d Caravanserai
Whose Doorways are alternate Night and Day,
  How Sultan after Sultan with his Pomp
Abode his Hour or two, and went his way.
""".strip()

text_item = hippo.CanvasText(text=text)
expand.append(text_item)

bottom = hippo.CanvasBox(id='bottom', orientation=hippo.ORIENTATION_HORIZONTAL, xalign=hippo.ALIGNMENT_CENTER)
root.append(bottom)

grippy = hippo.CanvasBox(id='grippy', box_width=20, box_height=8)
bottom.append(grippy)

expanded = True
def toggle_expanded():
    global expanded
    expanded = not expanded
    expand.animate(0.3333)
    text_item.set_visible(expanded)
    window.set_resizable(expanded)

header_button_down = False
header_button_down_x = 0;
header_button_down_y = 0;
    
def on_header_button_press(item, event):
    global header_button_down, header_button_down_x, header_button_down_y
    
    if event.button != 1:
        return

    if event.count == 2:
        toggle_expanded()
    elif event.count == 1:
        header_button_down = True
        header_button_down_x = event.x
        header_button_down_y = event.y

def on_header_button_release(item, event):
    global header_button_down, header_button_down_x, header_button_down_y
    
    if event.button != 1:
        return
    header_button_down = False

def begin_move():
    global header_button_down, header_button_down_x, header_button_down_y
    
    event = gtk.get_current_event()
    window.begin_move_drag(1,
                           int(round(0.5 + event.x_root)), int(round(event.y_root)),
                           event.time)
    
def on_header_motion_notify(item, event):
    if not header_button_down:
        return

    distance = max(abs(event.x - header_button_down_x), abs(event.x - header_button_down_x))
    double_click_distance = gtk.settings_get_for_screen(window.get_screen()).props.gtk_double_click_distance
    if distance > double_click_distance:
        begin_move()

header.connect('button-press-event', on_header_button_press)
header.connect('button-release-event', on_header_button_release)
header.connect('motion-notify-event', on_header_motion_notify)

def begin_resize(*args):
    event = gtk.get_current_event()
    window.begin_resize_drag(gtk.gdk.WINDOW_EDGE_SOUTH,
                             event.button,
                             int(round(0.5 + event.x_root)), int(round(event.y_root)),
                             event.time)

bottom.connect('button-press-event', begin_resize)

def on_header_right_motion_notify(item, event):
    if event.detail == hippo.MOTION_DETAIL_ENTER:
        item.animate(0.20)
        item.props.classes = 'prelight'
    if event.detail == hippo.MOTION_DETAIL_LEAVE:
        item.animate(0.20)
        item.props.classes = ''

header_right.connect('motion-notify-event', on_header_right_motion_notify)

header_right.connect('activated', lambda *args: gtk.main_quit())

width, _ = window.size_request()
window.set_size_request(width, -1)

window.show()

def toggle_timeout():
    toggle_expanded()
    return True

#gobject.timeout_add(4000, toggle_timeout)
#toggle_expanded()

gtk.main()
