import pygtk
pygtk.require('2.0')
import gtk
import hippo

window = gtk.Window()
window.connect("destroy", lambda w: gtk.main_quit())

canvas = hippo.Canvas()
window.add(canvas)
canvas.show()

root = hippo.CanvasBox()
canvas.set_root(root)
canvas.set_size_request(400, 400)

box1 = hippo.CanvasBox()

theme1 = hippo.CanvasTheme(theme_stylesheet="box1.css")

text = hippo.CanvasText(text="Box 1 theme", classes="title")
box1.append(text)

box1.set_theme(theme1)

root.append(box1)

box2 = hippo.CanvasBox()

theme2 = hippo.CanvasTheme(theme_stylesheet="box2.css")
box2.set_theme(theme2)

text = hippo.CanvasText(text="Box 2 theme", classes="title")
box2.append(text)

root.append(box2)

window.show()

gtk.main()
