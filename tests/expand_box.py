import cairo
import gtk
import gobject
import hippo

class _ExpandAnimation(hippo.Animation):
    __gsignals__ = { 'event': 'override' }
    
    def __init__(self, box, duration):
        super(_ExpandAnimation, self).__init__()
        
        self.__box = box
        self.add_event(0, duration)

    def do_event(self, id, fraction):
        if fraction == 1.0:
            self.__box._animation_finished()
        else:
            self.__box._animation_step(fraction)
        
class ExpandBox(hippo.CanvasBox, hippo.CanvasItem):
    __gtype_name__ = 'HippoExpandBox'

    def __init__(self, **kwargs):
        hippo.CanvasBox.__init__(self, **kwargs)
        self.__animation = None

    def do_get_width_request(self):
        minimum, natural = hippo.CanvasBox.do_get_width_request(self)
        if self.__animation:
            old_minimum, old_natural = self.__start_width_request
            fraction = self.__animation_fraction
            minimum = int(0.5 + minimum * fraction + old_minimum * (1 - fraction))
            natural = int(0.5 + natural * fraction + old_natural * (1 - fraction))

        return minimum, natural
    
    def do_get_height_request(self, for_width):
        minimum, natural = hippo.CanvasBox.do_get_height_request(self, for_width)
        if self.__animation:
            old_minimum, old_natural = self.__start_height_request
            fraction = self.__animation_fraction
            minimum = int(0.5 + minimum * fraction + old_minimum * (1 - fraction))
            natural = int(0.5 + natural * fraction + old_natural * (1 - fraction))

        return minimum, natural

    def do_paint(self, cr, rect):
        if self.__animation:
            new_surface = cr.get_target().create_similar(cairo.CONTENT_COLOR_ALPHA,
                                                         rect.width, rect.height)
            new_cr = gtk.gdk.CairoContext(cairo.Context(new_surface))
            new_cr.save()
            new_cr.set_operator(cairo.OPERATOR_CLEAR)
            new_cr.paint()
            new_cr.restore()
            new_cr.translate(- rect.x, - rect.y)
            hippo.CanvasBox.do_paint(self, new_cr, rect)

            tmp_surface = cr.get_target().create_similar(cairo.CONTENT_COLOR_ALPHA,
                                                         rect.width, rect.height)
            tmp_cr = gtk.gdk.CairoContext(cairo.Context(tmp_surface))
            
            tmp_cr.set_source_surface(self.__animation_surface, - rect.x, - rect.y)
            tmp_cr.set_operator(cairo.OPERATOR_SOURCE)
            tmp_cr.paint_with_alpha(1 - self.__animation_fraction)

            tmp_cr.set_source_surface(new_surface, 0, 0)
            tmp_cr.set_operator(cairo.OPERATOR_ADD)
            tmp_cr.paint_with_alpha(self.__animation_fraction)

            cr.set_source_surface(tmp_surface, rect.x, rect.y)
            cr.paint()
        else:
            hippo.CanvasBox.do_paint(self, cr, rect)

    def animate(self, duration):
        # First snapshot the current state (which may be an animated state)
        
        width, height = self.get_allocation()
        start_width_request = self.get_width_request()
        start_height_request = self.get_height_request(width)

        animation_surface = self.get_context().create_surface(cairo.CONTENT_COLOR_ALPHA,
                                                              width, height)
        cr = gtk.gdk.CairoContext(cairo.Context(animation_surface))
        cr.save()
        cr.set_operator(cairo.OPERATOR_CLEAR)
        cr.paint()
        cr.restore()
        r = hippo.Rectangle(0, 0, width, height)
        self.process_paint(cr,
                           hippo.Rectangle(0, 0, width, height),
                           0, 0)

        # Then cancel any current animation

        if self.__animation:
            self.__animation.cancel()
            self.__animation = None
            self.__animation_surface = None

        # And start the new animation from the current position
        
        self.__start_width_request = start_width_request
        self.__start_height_request = start_height_request

        self.__animation_surface = animation_surface

        self.__animation = _ExpandAnimation(self, duration)
        self.__animation_fraction = 0.
        self.get_animation_manager().add_animation(self.__animation)
        
    def _animation_step(self, fraction):
        self.__animation_fraction = fraction
        self.emit_request_changed()
        self.emit_paint_needed(0, 0, -1, -1)
    
    def _animation_finished(self):
        self.__animation = None
        self.__animation_surface = None
        self.emit_request_changed()

