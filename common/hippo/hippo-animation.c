/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */

#include "hippo-animation.h"
#include "hippo-canvas-marshal.h"

/* This defines the time before an event that counts as "in" the
 * event. The attempt here is to waking up fractionally early, then
 * sleeping again to get to the event
 */
#define FUZZ 0.01

typedef struct _AnimationEvent AnimationEvent;

struct _AnimationEvent {
    int id;
    double when;
    double duration;
};

#define EVENT(animation, id) ((AnimationEvent *)g_ptr_array_index((animation)->events, (id)))

enum {
    EVENT,
    CANCEL,
    LAST_SIGNAL
};

static int signals[LAST_SIGNAL];

G_DEFINE_TYPE(HippoAnimation, hippo_animation, G_TYPE_OBJECT)

static void
hippo_animation_finalize(GObject *object)
{
    HippoAnimation *animation = HIPPO_ANIMATION(object);
    guint i;
    
    for (i = 0; i < animation->events->len; i++) {
        AnimationEvent *event = EVENT(animation, i);
        g_free(event);
    }

    g_ptr_array_free(animation->events, TRUE);

    G_OBJECT_CLASS(hippo_animation_parent_class)->finalize(object);
}

static void
hippo_animation_init(HippoAnimation *animation)
{
    animation->position = 0;
    animation->first_current = 0;
    animation->events = g_ptr_array_new();
}

static void
hippo_animation_class_init(HippoAnimationClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = hippo_animation_finalize;
    
    /**
     * HippoAnimation::event
     *
     * Signal emitted when the animation is advanced to a new position.
     */
    signals[EVENT] =
        g_signal_new ("event",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET(HippoAnimationClass, event),
                      NULL, NULL,
                      hippo_canvas_marshal_VOID__INT_DOUBLE,
                      G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_DOUBLE);
    
    /**
     * HippoAnimation::cancel
     *
     * Signal emitted when the animation is cancelled
     */
    signals[CANCEL] =
        g_signal_new ("cancel",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET(HippoAnimationClass, event),
                      NULL, NULL,
                      hippo_canvas_marshal_VOID__NONE,
                      G_TYPE_NONE, 0);
}

/**
 * hippo_animation_new:
 * 
 * Return value: A newly created HippoAnimation object. Unref with g_object_unref()
 **/
HippoAnimation *
hippo_animation_new(void)
{
    return g_object_new(HIPPO_TYPE_ANIMATION, 0);
}

/**
 * hippo_animation_add_event:
 * @animation: the animation object
 * @when: time that this event starts, in seconds from the beginning of the animation
 * @duration: duration of the event, in seconds. If 0 or negative, the event is treated
 *   as instantaneous.
 *
 * Adds an event to the animation object.
 *
 * Return value: the integer ID for the animation. Will be passed to the ::event signal.
 **/
int
hippo_animation_add_event(HippoAnimation *animation,
                          double          when,
                          double          duration)
{
    AnimationEvent *event;
    int id;

    g_return_val_if_fail(HIPPO_IS_ANIMATION(animation), -1);

    id = animation->events->len;
    
    if ((id == 0 && when < 0) || (id > 0 && when < EVENT(animation, id - 1)->when)) {
        g_warning("Events must be added in time order");
        return -1;
    }

    event = g_new0(AnimationEvent, 1);

    event->id = id;
    event->when = when;
    event->duration = duration;

    g_ptr_array_add(animation->events, event);

    return event->id;
}

/**
 * hippo_animation_cancel:
 * @animation: the animation object
 *
 * Cancels the operation of the animation and removes it from the animation
 * manager it has been added to, if any.
 */
void
hippo_animation_cancel(HippoAnimation *animation)
{
    g_return_if_fail(HIPPO_IS_ANIMATION(animation));
    
    g_signal_emit(animation, signals[CANCEL], 0);
}

/**
 * hippo_animation_advance
 * @animation: the animation object
 * @position: the new position, in seconds from the beginning of the animation
 *
 * Advances the animation to a new position. This is function is meant for
 * HippoAnimationManager and is likely not useful for applications.
 */
void
hippo_animation_advance(HippoAnimation *animation,
                        double          position)
{
    gboolean seen_current = FALSE;
    guint i;

    for (i = 0; i < animation->events->len; i++) {
        AnimationEvent *event = EVENT(animation, i);

        if (event->when + event->duration < position) {
            if (!seen_current)
                animation->first_current = i + 1;
        } else {
            seen_current = TRUE;
        }

        if (event->when - FUZZ > position) /* future events */
            break;
                
        if (event->duration <= 0) { /*  1-time event */
            if (event->when - FUZZ > animation->position && event->when - FUZZ <= position)
                g_signal_emit(animation, signals[EVENT], 0, event->id, 0);
        } else { /* Continuous event */
            if (event->when <= position + FUZZ && event->when + event->duration >= position) {
                double fraction = MAX(0, (position - event->when) / event->duration);
                g_signal_emit(animation, signals[EVENT], 0, event->id, fraction);
            } else if (event->when + event->duration > animation->position && event->when + event->duration <= position) {
                /* Always give a final update at position 1.0 */
                g_signal_emit(animation, signals[EVENT], 0, event->id, 1.0);
            }
        }
    }
            
    animation->position = position;
}

/**
 * hippo_animation_advance
 * @animation: the animation object
 *
 * Gets the start time of the first event that is not yet finished.
 * This is function is meant for HippoAnimationManager and is likely not
 * useful for applications.
 *
 * Return Value: time in seconds from the beginning of the start of the first
 * still-current event. This time may be earlier than than the current time, which
 * means that an event that started before the current time is still going on.
 **/
double
hippo_animation_get_next_event_position(HippoAnimation *animation)
{
    AnimationEvent *event;
        
    if (animation->first_current == animation->events->len)
        return -1.;

    event = EVENT(animation, animation->first_current);

    return event->when;
}
