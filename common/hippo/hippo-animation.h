/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_ANIMATION_H__
#define __HIPPO_ANIMATION_H__

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * HippoAnimation::
 *
 * The HippoAnimation class represents a single animation that can be managed by
 * a HippoAnimationManager. An animation consists of a series of "events".
 * events can either be instantantaneous, or they can have a duration. Each
 * event is identified by an integer ID. (It was done this way rather than
 * using objects to avoid forcing someone implementation an animation to
 * define a new GObject type.)
 *
 * The base HippoAnimation class does nothing by itself. You can either
 * subclass it, or you can create an instance of the base class and connect
 * to the ::event and ::cancel signals.
 */

#define HIPPO_TYPE_ANIMATION              (hippo_animation_get_type ())
#define HIPPO_ANIMATION(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_ANIMATION, HippoAnimation))
#define HIPPO_ANIMATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_ANIMATION, HippoAnimationClass))
#define HIPPO_IS_ANIMATION(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_ANIMATION))
#define HIPPO_IS_ANIMATION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_ANIMATION))
#define HIPPO_ANIMATION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HIPPO_TYPE_ANIMATION, HippoAnimationClass))

typedef struct _HippoAnimation HippoAnimation;
typedef struct _HippoAnimationClass HippoAnimationClass;

struct _HippoAnimation {
    GObject parent;

    GPtrArray *events;
    guint first_current; /* Index of first event that is still current */
    double position; /* current time value */
};

struct _HippoAnimationClass {
    GObjectClass parent_class;

    void (*event) (HippoAnimation  *animation,
                   int              event_id,
                   double           fraction);
    void (*cancel) (HippoAnimation *animation);
};
 
GType             hippo_animation_get_type          (void) G_GNUC_CONST;

HippoAnimation *hippo_animation_new (void);

int hippo_animation_add_event(HippoAnimation *animation,
                              double          when,
                              double          duration);

void hippo_animation_cancel  (HippoAnimation *animation);

/* "Protected" methods used by HippoAnimationManager */
void   hippo_animation_advance                 (HippoAnimation *animation,
                                                double          position);
double hippo_animation_get_next_event_position (HippoAnimation *animation);

G_END_DECLS

#endif /* __HIPPO_ANIMATION_H__ */
