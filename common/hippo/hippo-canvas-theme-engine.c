/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#include <string.h>

#include "hippo-canvas-theme-engine.h"

static void     hippo_canvas_item_base_init (void                  *klass);

#if 0
enum {
    LAST_SIGNAL
};

static int signals[LAST_SIGNAL];
#endif

GType
hippo_canvas_theme_engine_get_type(void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info =
            {
                sizeof(HippoCanvasThemeEngineIface),
                hippo_canvas_item_base_init,
                NULL /* base_finalize */
            };
        type = g_type_register_static(G_TYPE_INTERFACE, "HippoCanvasThemeEngine",
                                      &info, 0);
        
        g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

    return type;
}

static void
hippo_canvas_item_base_init(void *klass)
{
    static gboolean initialized = FALSE;

   if (!initialized) {
        /* create signals in here */
    }
}

gboolean
hippo_canvas_theme_engine_paint (HippoCanvasThemeEngine *engine,
                                 HippoCanvasStyle       *style,
                                 cairo_t                *cr,
                                 const char             *name,
                                 double                  x,
                                 double                  y,
                                 double                  width,
                                 double                  height)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_THEME_ENGINE(engine), FALSE);
    
    return HIPPO_CANVAS_THEME_ENGINE_GET_IFACE(engine)->paint(engine,
                                                              style, cr, name,
                                                              x, y, width, height);
}

