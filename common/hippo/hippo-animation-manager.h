/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_ANIMATION_MANAGER_H__
#define __HIPPO_ANIMATION_MANAGER_H__

#include <hippo/hippo-animation.h>

G_BEGIN_DECLS

/**
 * HippoAnimationManager::
 *
 * The animation manager class takes care of running animations within a single canvas.
 * While there are active animations, the animation manager advances all the active
 * animations, then emits the ::after-frame signal. The canvas container (which will
 * create and own the animation manager) watches for that signal, and when it sees
 * it waits until all resizing and drawing is complete and calls
 * hippo_animation_manager_frame_complete(). (The frame serial from ::after-frame
 * should be passed to frame_complete() though the concept of frame serials isn't
 * really fully worked out here.)
 */

#define HIPPO_TYPE_ANIMATION_MANAGER              (hippo_animation_manager_get_type ())
#define HIPPO_ANIMATION_MANAGER(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_ANIMATION_MANAGER, HippoAnimationManager))
#define HIPPO_ANIMATION_MANAGER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_ANIMATION_MANAGER, HippoAnimationManagerClass))
#define HIPPO_IS_ANIMATION_MANAGER(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_ANIMATION_MANAGER))
#define HIPPO_IS_ANIMATION_MANAGER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_ANIMATION_MANAGER))
#define HIPPO_ANIMATION_MANAGER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HIPPO_TYPE_ANIMATION_MANAGER, HippoAnimationManagerClass))

typedef struct _HippoAnimationManager HippoAnimationManager;
typedef struct _HippoAnimationManagerClass HippoAnimationManagerClass;

GType             hippo_animation_manager_get_type          (void) G_GNUC_CONST;

HippoAnimationManager *hippo_animation_manager_new (void);

void hippo_animation_manager_add_animation    (HippoAnimationManager *manager,
                                               HippoAnimation        *animation,
                                               double                 delay);
void hippo_animation_manager_frame_complete   (HippoAnimationManager *manager,
                                               guint                  frame_serial);

G_END_DECLS

#endif /* __HIPPO_ANIMATION_MANAGER_H__ */
