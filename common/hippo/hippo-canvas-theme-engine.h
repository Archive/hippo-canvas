/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_THEME_ENGINE_H__
#define __HIPPO_CANVAS_THEME_ENGINE_H__

#include <cairo.h>
#include <hippo/hippo-event.h>
#include <hippo/hippo-graphics.h>
#include <hippo/hippo-canvas-style.h>

G_BEGIN_DECLS

typedef struct _HippoCanvasThemeEngine      HippoCanvasThemeEngine;
typedef struct _HippoCanvasThemeEngineIface HippoCanvasThemeEngineIface;

#define HIPPO_TYPE_CANVAS_THEME_ENGINE              (hippo_canvas_theme_engine_get_type ())
#define HIPPO_CANVAS_THEME_ENGINE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_CANVAS_THEME_ENGINE, HippoCanvasThemeEngine))
#define HIPPO_IS_CANVAS_THEME_ENGINE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_CANVAS_THEME_ENGINE))
#define HIPPO_CANVAS_THEME_ENGINE_GET_IFACE(obj)    (G_TYPE_INSTANCE_GET_INTERFACE ((obj), HIPPO_TYPE_CANVAS_THEME_ENGINE, HippoCanvasThemeEngineIface))

struct _HippoCanvasThemeEngineIface {
    GTypeInterface base_iface;

    gboolean               (* paint)                (HippoCanvasThemeEngine *engine,
                                                     HippoCanvasStyle       *style,
                                                     cairo_t                *cr,
                                                     const char             *name,
                                                     double                  x,
                                                     double                  y,
                                                     double                  width,
                                                     double                  height);
};

GType hippo_canvas_theme_engine_get_type (void) G_GNUC_CONST;

gboolean hippo_canvas_theme_engine_paint (HippoCanvasThemeEngine *engine,
                                          HippoCanvasStyle       *style,
                                          cairo_t                *cr,
                                          const char             *name,
                                          double                  x,
                                          double                  y,
                                          double                  width,
                                          double                  height);

G_END_DECLS

#endif /* __HIPPO_CANVAS_THEME_ENGINE_H__ */
