/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */

#include <stdlib.h>
#include <string.h>

#include "hippo-canvas-internal.h"
#include "hippo-canvas-theme-internal.h"
#include "hippo-canvas-type-builtins.h"
#include "hippo-canvas-style.h"

static void hippo_canvas_style_init               (HippoCanvasStyle          *style);
static void hippo_canvas_style_class_init         (HippoCanvasStyleClass     *klass);
static void hippo_canvas_style_dispose            (GObject                 *object);
static void hippo_canvas_style_finalize           (GObject                 *object);


#if 0
enum {
    LAST_SIGNAL
};

static int signals[LAST_SIGNAL];
#endif

struct _HippoCanvasStyle {
    GObject parent;

    HippoCanvasContext *context;
    HippoCanvasStyle *parent_style;
    HippoCanvasTheme *theme;
    
    PangoFontDescription *font_desc;

    guint32 background_color;
    guint32 foreground_color;
    guint32 border_color[4];
    double border_width[4];
    guint padding[4];

    HippoCanvasThemeImage *background_theme_image;

    GType element_type;
    char *element_id;
    char *element_class;

    CRDeclaration **properties;
    int n_properties;
    
    guint properties_computed : 1;
    guint borders_computed : 1;
    guint background_computed : 1;
    guint foreground_computed : 1;
    guint background_theme_image_computed : 1;
    guint link_type : 2;
};

struct _HippoCanvasStyleClass {
    GObjectClass parent_class;

};

G_DEFINE_TYPE(HippoCanvasStyle, hippo_canvas_style, G_TYPE_OBJECT)

static void
hippo_canvas_style_init(HippoCanvasStyle *style)
{
    style->link_type = HIPPO_CANVAS_LINK_NONE;
}

static void
hippo_canvas_style_class_init(HippoCanvasStyleClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = hippo_canvas_style_dispose;
    object_class->finalize = hippo_canvas_style_finalize;
}

static void
hippo_canvas_style_dispose(GObject *object)
{
    /* HippoCanvasStyle *style = HIPPO_CANVAS_STYLE(object); */

    G_OBJECT_CLASS(hippo_canvas_style_parent_class)->dispose(object);
}

static void
hippo_canvas_style_finalize(GObject *object)
{
    HippoCanvasStyle *style = HIPPO_CANVAS_STYLE(object);

    g_free (style->element_id);
    g_free (style->element_class);

    if (style->properties) {
        g_free(style->properties);
        style->properties = NULL;
        style->n_properties = 0;
    }

    if (style->font_desc) {
        pango_font_description_free(style->font_desc);
        style->font_desc = NULL;
    }

    if (style->background_theme_image) {
        g_object_unref(style->background_theme_image);
        style->background_theme_image = NULL;
    }

    G_OBJECT_CLASS(hippo_canvas_style_parent_class)->finalize(object);
}

HippoCanvasStyle *
hippo_canvas_style_new (HippoCanvasContext    *context,
                        HippoCanvasStyle      *parent_style,
                        HippoCanvasTheme      *theme,
                        GType                  element_type,
                        const char            *element_id,
                        const char            *element_class)
{
    HippoCanvasStyle *style;
    
    g_return_val_if_fail(HIPPO_IS_CANVAS_CONTEXT(context), NULL);
    g_return_val_if_fail(parent_style == NULL || HIPPO_IS_CANVAS_STYLE(parent_style), NULL);

    style = g_object_new(HIPPO_TYPE_CANVAS_STYLE, NULL);
    
    style->context = g_object_ref(context);
    if (parent_style != NULL)
        style->parent_style = g_object_ref(parent_style);
    else
        style->parent_style = NULL;

    if (theme == NULL && parent_style != NULL)
        theme = parent_style->theme;
    
    if (theme != NULL)
        style->theme = g_object_ref(theme);

    style->element_type = element_type;
    style->element_id = g_strdup(element_id);
    style->element_class = g_strdup(element_class);

    return style;
}

void
hippo_canvas_style_set_link_type (HippoCanvasStyle    *style,
                                  HippoCanvasLinkType  link_type)
{
    g_return_if_fail(HIPPO_IS_CANVAS_STYLE(style));

    style->link_type = link_type;
}

HippoCanvasLinkType
hippo_canvas_style_get_link_type (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), HIPPO_CANVAS_LINK_NONE);
    
    return style->link_type;
}

HippoCanvasStyle *
hippo_canvas_style_get_parent (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), NULL);
    
    return style->parent_style;
}

HippoCanvasTheme *
hippo_canvas_style_get_theme (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), NULL);
    
    return style->theme;
}

GType
hippo_canvas_style_get_element_type (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), G_TYPE_NONE);

    return style->element_type;
}

const char *
hippo_canvas_style_get_element_id (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), NULL);

    return style->element_id;
}

const char *
hippo_canvas_style_get_element_class (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), NULL);

    return style->element_class;
}

static void
ensure_properties(HippoCanvasStyle *style)
{
    if (!style->properties_computed) {
        style->properties_computed = TRUE;

        if (style->theme)
            _hippo_canvas_theme_get_matched_properties(style->theme, style,
                                                       &style->properties, &style->n_properties);
    }
}

typedef enum {
    VALUE_FOUND,
    VALUE_NOT_FOUND,
    VALUE_INHERIT
} GetFromTermResult;

static int
color_component_from_double(double component)
{
    /* http://people.redhat.com/otaylor/pixel-converting.html */
    if (component >= 1.0)
        return 255;
    else
        return (int)(component * 256);
}

static GetFromTermResult
get_color_from_rgba_term(CRTerm           *term,
                         guint32          *color)
{
    CRTerm *arg = term->ext_content.func_param;
    CRNum *num;
    double r = 0, g = 0, b = 0, a = 0;
    int i;

    for (i = 0; i < 4; i++) {
        double value;
            
        if (arg == NULL)
            return VALUE_NOT_FOUND;
            
        if ((i == 0 && arg->the_operator != NO_OP) ||
            (i > 0 && arg->the_operator != COMMA))
            return VALUE_NOT_FOUND;

        if (arg->type != TERM_NUMBER)
            return VALUE_NOT_FOUND;
        
        num = arg->content.num;

        /* For simplicity, we convert a,r,g,b to [0,1.0] floats and then
         * convert them back below. Then when we set them on a cairo content
         * we convert them back to floats, and then cairo converts them
         * back to integers to pass them to X, and so forth...
         */
        if (i < 3) {
            if (num->type == NUM_PERCENTAGE) {
                value = num->val / 100;
            } else if (num->type == NUM_GENERIC) {
                value = num->val / 255;
            } else {
                return VALUE_NOT_FOUND;
            }
        } else {
            if (num->type != NUM_GENERIC)
                return VALUE_NOT_FOUND;

            value = num->val;
        }

        value = CLAMP(value, 0, 1);

        switch (i) {
        case 0:
            r = value;
            break;
        case 1:
            g = value;
            break;
        case 2:
            b = value;
            break;
        case 3:
            a = value;
            break;
        }
        
        arg = arg->next;
    }

    *color = ((color_component_from_double(r) << 24) |
              (color_component_from_double(g) << 16) |
              (color_component_from_double(b) << 8) |
              ((color_component_from_double(a))));
               
    return VALUE_FOUND;
}
                         
static GetFromTermResult
get_color_from_term(HippoCanvasStyle *style,
                    CRTerm           *term,
                    guint32          *color)
{
    CRRgb rgb;
    enum CRStatus status;

    /* Since libcroco doesn't know about rgba colors, it can't handle
     * the transparent keyword
     */
    if (term->type == TERM_IDENT &&
        term->content.str &&
        term->content.str->stryng &&
        term->content.str->stryng->str &&
        strcmp(term->content.str->stryng->str, "transparent") == 0)
    {
        *color = 0;
        return VALUE_FOUND;
    }
    /* rgba() colors - a CSS3 addition, are not supported by libcroco,
     * but they are parsed as a "function", so we can emulate the
     * functionality.
     */
    else if (term->type == TERM_FUNCTION &&
             term->content.str &&
             term->content.str->stryng &&
             term->content.str->stryng->str &&
             strcmp(term->content.str->stryng->str, "rgba") == 0)
    {
        return get_color_from_rgba_term(term, color);
    }

    status = cr_rgb_set_from_term(&rgb, term);
    if (status != CR_OK)
        return VALUE_NOT_FOUND;
            
    if (rgb.inherit)
        return VALUE_INHERIT;
            
    if (rgb.is_percentage)
        cr_rgb_compute_from_percentage(&rgb);
            
    *color = (rgb.red << 24) | (rgb.green << 16) | (rgb.blue << 8) | 0xff;

    return VALUE_FOUND;
}

gboolean
hippo_canvas_style_get_color (HippoCanvasStyle     *style,
                              const char           *property_name,
                              gboolean              inherit,
                              guint32              *color)
{

    int i;

    ensure_properties(style);

    for (i = style->n_properties - 1; i >= 0; i--) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, property_name) == 0) {
            GetFromTermResult result = get_color_from_term(style, decl->value, color);
            if (result == VALUE_FOUND) {
                return TRUE;
            } else if (result == VALUE_INHERIT) {
                if (style->parent_style)
                    return hippo_canvas_style_get_color(style->parent_style, property_name, inherit, color);
                else
                    break;
            }
        }
    }

    return FALSE;
}

gboolean
hippo_canvas_style_get_double (HippoCanvasStyle     *style,
                               const char           *property_name,
                               gboolean              inherit,
                               double               *value)
{
    gboolean result = FALSE;
    int i;

    ensure_properties(style);

    for (i = style->n_properties - 1; i >= 0; i--) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, property_name) == 0) {
            CRTerm *term = decl->value;

            if (term->type != TERM_NUMBER || term->content.num->type != NUM_GENERIC)
                continue;

            *value = term->content.num->val;
            result = TRUE;
            break;
        }
    }

    if (!result && inherit && style->parent_style)
        result = hippo_canvas_style_get_double(style->parent_style, property_name, inherit, value);

    return result;
}

static PangoFontDescription *
get_parent_font(HippoCanvasStyle *style)
{
    if (style->parent_style)
        return hippo_canvas_style_get_font(style->parent_style);
    else
        return hippo_canvas_context_get_font(style->context);
}

static GetFromTermResult
get_length_from_term(HippoCanvasStyle *style,
                     CRTerm           *term,
                     gboolean          use_parent_font,
                     gdouble          *length)
{
    CRNum *num;
            
    enum {
        ABSOLUTE,
        POINTS,
        FONT_RELATIVE,
    } type = ABSOLUTE;
    
    double multiplier = 1.0;
    
    if (term->type != TERM_NUMBER) {
        g_warning("Ignoring length property that isn't a number");
        return FALSE;
    }
    
    num = term->content.num;
    
    switch (num->type) {
    case NUM_LENGTH_PX:
        type = ABSOLUTE;
        multiplier = 1;
        break;
    case NUM_LENGTH_PT:
        type = POINTS;
        multiplier = 1;
        break;
    case NUM_LENGTH_IN:
        type = POINTS;
        multiplier = 72;
        break;
    case NUM_LENGTH_CM:
        type = POINTS;
        multiplier = 72. / 2.54;
        break;
    case NUM_LENGTH_MM:
        type = POINTS;
        multiplier = 72. / 25.4;
        break;
    case NUM_LENGTH_PC:
        type = POINTS;
        multiplier = 12. / 25.4;
        break;
    case NUM_LENGTH_EM:
        {
            type = FONT_RELATIVE;
            multiplier = 1;
            break; 
        }
    case NUM_LENGTH_EX:
        {
            /* Doing better would require actually resolving the font description
             * to a specific font, and Pango doesn't have an ex metric anyways,
             * so we'd have to try and synthesize it by complicated means.
             *
             * The 0.5em is the CSS spec suggested thing to use when nothing
             * better is available.
             */
            type = FONT_RELATIVE;
            multiplier = 0.5;
            break; 
        }
        
    case NUM_INHERIT:
        return VALUE_INHERIT;
        
    case NUM_AUTO:
        g_warning("'auto' not supported for lengths");
        return VALUE_NOT_FOUND;
        
    case NUM_GENERIC:
        g_warning("length values must specify a unit");
        return VALUE_NOT_FOUND;
        
    case NUM_PERCENTAGE:
        g_warning("percentage lengths not currently supported");
        return VALUE_NOT_FOUND;
        
    case NUM_ANGLE_DEG:
    case NUM_ANGLE_RAD:
    case NUM_ANGLE_GRAD:
    case NUM_TIME_MS:
    case NUM_TIME_S:
    case NUM_FREQ_HZ:
    case NUM_FREQ_KHZ:
    case NUM_UNKNOWN_TYPE:
    case NB_NUM_TYPE:
        g_warning("Ignoring invalid type of number of length property");
        return VALUE_NOT_FOUND;
    }
    
    switch (type) {
    case ABSOLUTE:
        *length = num->val * multiplier;
        break;
    case POINTS:
        {
            double resolution = hippo_canvas_context_get_resolution(style->context);
            *length = num->val * multiplier * (resolution / 72.);
        }
        break;
    case FONT_RELATIVE:
        {
            PangoFontDescription *desc;
            double font_size;
            
            if (use_parent_font)
                desc = get_parent_font(style);
            else
                desc = hippo_canvas_style_get_font(style);

            font_size = (double)pango_font_description_get_size(desc) / PANGO_SCALE;

            if (pango_font_description_get_size_is_absolute(desc)) {
                *length = num->val * multiplier * font_size;
            } else {
                double resolution = hippo_canvas_context_get_resolution(style->context);
                *length = num->val * multiplier * (resolution / 72.) * font_size;
            }
        }
        break;
    default:
        g_assert_not_reached();
    }
    
    return VALUE_FOUND;
}

static GetFromTermResult
get_length_internal(HippoCanvasStyle     *style,
                    const char           *property_name,
                    const char           *suffixed,
                    gdouble              *length)
{
    int i;

    ensure_properties(style);

    for (i = style->n_properties - 1; i >= 0; i--) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, property_name) == 0 ||
            (suffixed != NULL && strcmp(decl->property->stryng->str, suffixed) == 0)) {
            GetFromTermResult result = get_length_from_term(style, decl->value, FALSE, length);
            if (result != VALUE_NOT_FOUND)
                return result;
        }
    }

    return VALUE_NOT_FOUND;
}

gboolean
hippo_canvas_style_get_length (HippoCanvasStyle     *style,
                               const char           *property_name,
                               gboolean              inherit,
                               gdouble              *length)
{
    GetFromTermResult result = get_length_internal(style, property_name, NULL, length);
    if (result == VALUE_FOUND)
        return TRUE;
    else if (result == VALUE_INHERIT)
        inherit = TRUE;

    if (inherit && style->parent_style &&
        hippo_canvas_style_get_length(style->parent_style, property_name, inherit, length))
        return TRUE;
    else
        return FALSE;
}

static void
do_border_property(HippoCanvasStyle *style,
                   CRDeclaration    *decl)
{
    const char *property_name = decl->property->stryng->str + 6; /* Skip 'border' */
    HippoCanvasSide side = (HippoCanvasSide)-1;
    guint32 color;
    gboolean color_set = FALSE;
    double width;
    gboolean width_set = FALSE;
    int j;

    if (g_str_has_prefix(property_name, "-left")) {
        side = HIPPO_CANVAS_SIDE_LEFT;
        property_name += 5;
    } else if (g_str_has_prefix(property_name, "-right")) {
        side = HIPPO_CANVAS_SIDE_RIGHT;
        property_name += 6;
    } else if (g_str_has_prefix(property_name, "-top")) {
        side = HIPPO_CANVAS_SIDE_TOP;
        property_name += 4;
    } else if (g_str_has_prefix(property_name, "-bottom")) {
        side = HIPPO_CANVAS_SIDE_BOTTOM;
        property_name += 7;
    }
    
    if (strcmp(property_name, "") == 0) {
        /* Set value for width/color/style in any order */
        CRTerm *term;
        
        for (term = decl->value; term; term = term->next) {
            GetFromTermResult result;
            
            if (term->type == TERM_IDENT) {
                const char *ident = term->content.str->stryng->str;
                if (strcmp(ident, "none") == 0 || strcmp(ident, "hidden") == 0) {
                    width = 0.;
                    continue;
                } else if (strcmp(ident, "solid") == 0) {
                    /* The only thing we support */
                    continue;
                } else if (strcmp(ident, "dotted") == 0 ||
                           strcmp(ident, "dashed") == 0 ||
                           strcmp(ident, "solid") == 0 ||
                           strcmp(ident, "double") == 0 ||
                           strcmp(ident, "groove") == 0 ||
                           strcmp(ident, "ridge") == 0 ||
                           strcmp(ident, "inset") == 0 ||
                           strcmp(ident, "outset") == 0) {
                    /* Treat the same as solid */
                    continue;
                }
                
                /* Presumably a color, fall through */
            }
            
            if (term->type == TERM_NUMBER) {
                result = get_length_from_term(style, term, FALSE, &width);
                if (result != VALUE_NOT_FOUND) {
                    width_set = result == VALUE_FOUND;
                    continue;
                }
            }
            
            result = get_color_from_term(style, term, &color);
            if (result != VALUE_NOT_FOUND) {
                color_set = result == VALUE_FOUND;
                continue;
            }
        }
        
    } else if (strcmp(property_name, "-color") == 0) {
        if (decl->value == NULL || decl->value->next != NULL)
            return;
        
        if (get_color_from_term(style, decl->value, &color) == VALUE_FOUND) { /* Ignore inherit */
            color_set = TRUE;
        }
    } else if (strcmp(property_name, "-width") == 0) {
        if (decl->value == NULL || decl->value->next != NULL)
            return;
        
        if (get_length_from_term(style, decl->value, FALSE, &width) == VALUE_FOUND) { /* Ignore inherit */
            width_set = TRUE;
        }
    }
    
    if (side == (HippoCanvasSide)-1) {
        for (j = 0; j < 4; j++) {
            if (color_set)
                style->border_color[j] = color;
            if (width_set)
                style->border_width[j] = width;
        }
    } else {
        if (color_set)
            style->border_color[side] = color;
        if (width_set)
            style->border_width[side] = width;
    }
}

static void
do_padding_property_term(HippoCanvasStyle *style,
                         CRTerm           *term,
                         gboolean          left,
                         gboolean          right,
                         gboolean          top,
                         gboolean          bottom)
{
    gdouble value;

    if (get_length_from_term(style, term, FALSE, &value) != VALUE_FOUND) 
        return;
    
    if (left)
        style->padding[HIPPO_CANVAS_SIDE_LEFT] = value;
    if (right)
        style->padding[HIPPO_CANVAS_SIDE_RIGHT] = value;
    if (top)
        style->padding[HIPPO_CANVAS_SIDE_TOP] = value;
    if (bottom)
        style->padding[HIPPO_CANVAS_SIDE_BOTTOM] = value;
}

static void
do_padding_property(HippoCanvasStyle *style,
                    CRDeclaration    *decl)
{
    const char *property_name = decl->property->stryng->str + 7; /* Skip 'padding' */

    if (strcmp(property_name, "") == 0) {
        /* Slight deviation ... if we don't understand some of the terms and understand others,
         * then we set the ones we understand and ignore the others instead of ignoring the
         * whole thing
         */
        if (decl->value == NULL) /* 0 values */
            return;
        else if (decl->value->next == NULL) { /* 1 value */
            do_padding_property_term(style, decl->value, TRUE, TRUE, TRUE, TRUE); /* left/right/top/bottom */
            return;
        }  else if (decl->value->next->next == NULL) { /* 2 values */
            do_padding_property_term(style, decl->value,       FALSE, FALSE, TRUE,  TRUE);  /* top/bottom */
            do_padding_property_term(style, decl->value->next, TRUE, TRUE,   FALSE, FALSE); /* left/right */
        }  else if (decl->value->next->next->next == NULL) { /* 3 values */
            do_padding_property_term(style, decl->value,             FALSE, FALSE, TRUE,  FALSE); /* top */
            do_padding_property_term(style, decl->value->next,       TRUE,  TRUE,  FALSE, FALSE); /* left/right */
            do_padding_property_term(style, decl->value->next->next, FALSE, FALSE, FALSE, TRUE);  /* bottom */
        } else  if (decl->value->next->next->next == NULL) { /* 4 values */
            do_padding_property_term(style, decl->value,                   FALSE, FALSE, TRUE,  FALSE);
            do_padding_property_term(style, decl->value->next,             FALSE, TRUE, FALSE, FALSE); /* left */
            do_padding_property_term(style, decl->value->next->next,       FALSE, FALSE, FALSE, TRUE);
            do_padding_property_term(style, decl->value->next->next->next, TRUE,  FALSE, FALSE, TRUE); /* left */
        } else {
            g_warning("Too many values for padding property");
            return;
        }
    } else {
        if (decl->value == NULL || decl->value->next != NULL)
            return;
        
        if (strcmp(property_name, "-left") == 0) {
            do_padding_property_term(style, decl->value, TRUE,  FALSE, FALSE, FALSE);
        } else if (strcmp(property_name, "-right") == 0) {
            do_padding_property_term(style, decl->value, FALSE, TRUE,  FALSE, FALSE);
        } else if (strcmp(property_name, "-top") == 0) {
            do_padding_property_term(style, decl->value, FALSE, FALSE, TRUE,  FALSE);
        } else if (strcmp(property_name, "-bottom") == 0) {
            do_padding_property_term(style, decl->value, FALSE, FALSE, FALSE, TRUE);
        }
    }
}

static void
ensure_borders(HippoCanvasStyle *style)
{
    int i, j;
    
    if (style->borders_computed)
        return;

    style->borders_computed = TRUE;
    
    ensure_properties(style);

    for (j = 0; j < 4; j++) {
        style->border_width[j] = 0;
        style->border_color[j] = 0x00000000;
    }
    
    for (i = 0; i < style->n_properties; i++) {
        CRDeclaration *decl = style->properties[i];
        const char *property_name = decl->property->stryng->str;

        if (g_str_has_prefix(property_name, "border")) {
            do_border_property(style, decl);
        } else if (g_str_has_prefix(property_name, "padding")) {
            do_padding_property(style, decl);
        }
    }
}

double
hippo_canvas_style_get_border_width(HippoCanvasStyle *style,
                                    HippoCanvasSide   side)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), 0.);
    g_return_val_if_fail(side >= HIPPO_CANVAS_SIDE_LEFT && side <= HIPPO_CANVAS_SIDE_BOTTOM, 0.);
    
    ensure_borders(style);

    return style->border_width[side];
}

static GetFromTermResult
get_background_color_from_term(HippoCanvasStyle *style,
                               CRTerm           *term,
                               guint32          *color)
{
    GetFromTermResult result = get_color_from_term(style, term, color);
    if (result == VALUE_NOT_FOUND) {
        if (term->type == TERM_IDENT &&
            strcmp(term->content.str->stryng->str, "transparent") == 0)
        {
            *color = 0x00000000;
            return VALUE_FOUND;
        }
    }

    return result;
}

guint32
hippo_canvas_style_get_background_color (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), 0);
    
    if (!style->background_computed) {
        int i;
        
        style->background_computed = TRUE;
        style->background_color = 0x00000000; /* Transparent */
        
        ensure_properties(style);
        
        for (i = style->n_properties - 1; i >= 0; i--) {
            CRDeclaration *decl = style->properties[i];
            const char *property_name = decl->property->stryng->str;
            
            if (g_str_has_prefix(property_name, "background"))
                property_name += 10;
            else
                continue;

            if (strcmp(property_name, "") == 0) {
                /* We're very liberal here ... if any term in the expression we take it, and 
                 * we ignore the rest. The actual specification is:
                 *
                 * background: [<'background-color'> || <'background-image'> || <'background-repeat'> || <'background-attachment'> || <'background-position'>] | inherit
                 */

                CRTerm *term;
                for (term = decl->value; term; term = term->next) {
                    GetFromTermResult result = get_background_color_from_term(style, term, &style->background_color);
                    if (result == VALUE_FOUND) {
                        return style->background_color;
                    } else if (result == VALUE_INHERIT) {
                        if (style->parent_style)
                            style->background_color = hippo_canvas_style_get_background_color(style->parent_style);

                        return style->background_color;
                    }
                }
                
            } else if (strcmp(property_name, "-color") == 0) {
                GetFromTermResult result;
                
                if (decl->value == NULL || decl->value->next != NULL)
                    continue;

                result = get_background_color_from_term(style, decl->value, &style->background_color);
                if (result == VALUE_FOUND) {
                    return style->background_color;
                } else if (result == VALUE_INHERIT) {
                    if (style->parent_style)
                        style->background_color = hippo_canvas_style_get_background_color(style->parent_style);
                    
                    return style->background_color;
                }
            }
        }
    }
     
    return style->background_color;
}

guint32
hippo_canvas_style_get_foreground_color (HippoCanvasStyle *style)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), 0);
    
    if (!style->foreground_computed) {
        int i;

        style->foreground_computed = TRUE;
        
        ensure_properties(style);
        
        for (i = style->n_properties - 1; i >= 0; i--) {
            CRDeclaration *decl = style->properties[i];
            
            if (strcmp(decl->property->stryng->str, "color") == 0) {
                GetFromTermResult result = get_color_from_term(style, decl->value, &style->foreground_color);
                if (result == VALUE_FOUND) {
                    return style->foreground_color;
                } else if (result == VALUE_INHERIT) {
                    break;
                }
            }
        }

        if (style->parent_style)
            style->foreground_color = hippo_canvas_style_get_foreground_color(style->parent_style);
        else
            style->foreground_color = hippo_canvas_context_get_color(style->context, HIPPO_STOCK_COLOR_FG);
    }
     
    return style->foreground_color;
    
}

guint32
hippo_canvas_style_get_border_color(HippoCanvasStyle *style,
                                    HippoCanvasSide   side)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), 0);
    g_return_val_if_fail(side >= HIPPO_CANVAS_SIDE_LEFT && side <= HIPPO_CANVAS_SIDE_BOTTOM, 0.);
    
    ensure_borders(style);

    return style->border_color[side];
}

double
hippo_canvas_style_get_padding(HippoCanvasStyle *style,
                               HippoCanvasSide   side)
{
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), 0.);
    g_return_val_if_fail(side >= HIPPO_CANVAS_SIDE_LEFT && side <= HIPPO_CANVAS_SIDE_BOTTOM, 0.);
    
    ensure_borders(style);

    return style->padding[side];
}

HippoTextDecoration
hippo_canvas_style_get_text_decoration (HippoCanvasStyle *style)
{
    int i;

    ensure_properties(style);

    for (i = style->n_properties - 1; i >= 0; i--) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, "text-decoration") == 0) {
            CRTerm *term = decl->value;
            HippoTextDecoration decoration = 0;

            /* Specification is none | [ underline || overline || line-through || blink ] | inherit
             *
             * We're a bit more liberal, and for example treat 'underline none' as the same as
             * none.
             */
            for (; term; term = term->next) {
                if (term->type != TERM_IDENT)
                    goto next_decl;

                if (strcmp(term->content.str->stryng->str, "none") == 0) {
                    return 0;
                } else if (strcmp(term->content.str->stryng->str, "inherit") == 0) {
                    if (style->parent_style)
                        return hippo_canvas_style_get_text_decoration(style->parent_style);
                } else if (strcmp(term->content.str->stryng->str, "underline") == 0) {
                    decoration |= HIPPO_TEXT_DECORATION_UNDERLINE;
                } else if (strcmp(term->content.str->stryng->str, "overline") == 0) {
                    decoration |= HIPPO_TEXT_DECORATION_OVERLINE;
                } else if (strcmp(term->content.str->stryng->str, "line-through") == 0) {
                    decoration |= HIPPO_TEXT_DECORATION_LINE_THROUGH;
                } else if (strcmp(term->content.str->stryng->str, "blink") == 0) {
                    decoration |= HIPPO_TEXT_DECORATION_BLINK;
                } else {
                    goto next_decl;
                }
            }

            return decoration;
        }

    next_decl:
        ;
    }

    return 0;
}

static gboolean
font_family_from_terms(CRTerm *term,
                       char  **family)
{
    GString *family_string;
    gboolean result = FALSE;
    gboolean last_was_quoted = FALSE;
    
    if (!term)
        return FALSE;

    family_string = g_string_new(NULL);

    while (term) {
        if (term->type != TERM_STRING && term->type != TERM_IDENT) {
            goto out;
        }

        if (family_string->len > 0) {
            if (term->the_operator != COMMA && term->the_operator != NO_OP)
                goto out;
            /* Can concetenate to bare words, but not two quoted strings */
            if ((term->the_operator == NO_OP && last_was_quoted) || term->type == TERM_STRING)
                goto out;

            if (term->the_operator == NO_OP) {
                g_string_append(family_string, " ");
            } else {
                g_string_append(family_string, ", ");
            }
        } else {
            if (term->the_operator != NO_OP)
                goto out;
        }

        g_string_append(family_string, term->content.str->stryng->str);
        
        term = term->next;
    }

    result = TRUE;

 out:
    if (result) {
        *family = g_string_free(family_string, FALSE);
        return TRUE;
    } else {
        *family = g_string_free(family_string, TRUE);
        return FALSE;
    }
}

/* In points */
static int font_sizes[] = {
    6 * 1024,   /* xx-small */
    8 * 1024,   /* x-small */
    10 * 1024,  /* small */
    12 * 1024,  /* medium */
    16 * 1024,  /* large */
    20 * 1024,  /* x-large */
    24 * 1024,  /* xx-large */
};

static gboolean
font_size_from_term(HippoCanvasStyle *style,
                    CRTerm           *term,
                    double           *size)
{
    if (term->type == TERM_IDENT) {
        double resolution = hippo_canvas_context_get_resolution(style->context);
        /* We work in integers to avoid double comparisons when converting back
         * from a size in pixels to a logical size.
         */
        int size_points = (int)(0.5 + *size * (72. / resolution));
        
        if (strcmp(term->content.str->stryng->str, "xx-small") == 0) {
            size_points = font_sizes[0];
        } else if (strcmp(term->content.str->stryng->str, "x-small") == 0) {
            size_points = font_sizes[1];
        } else if (strcmp(term->content.str->stryng->str, "small") == 0) {
            size_points = font_sizes[2];
        } else if (strcmp(term->content.str->stryng->str, "medium") == 0) {
            size_points = font_sizes[3];
        } else if (strcmp(term->content.str->stryng->str, "large") == 0) {
            size_points = font_sizes[4];
        } else if (strcmp(term->content.str->stryng->str, "x-large") == 0) {
            size_points = font_sizes[5];
        } else if (strcmp(term->content.str->stryng->str, "xx-large") == 0) {
            size_points = font_sizes[6];
        } else if (strcmp(term->content.str->stryng->str, "smaller") == 0) {
            /* Find the standard size equal to or smaller than the current size */
            int i = 0;

            while (i <= 6 && font_sizes[i] < size_points)
                i++;
            
            if (i > 6) { /* original size greater than any standard size */
                size_points = (int)(0.5 + size_points / 1.2);
            } else {
                /* Go one smaller than that, if possible */
                if (i > 0)
                    i--;

                size_points = font_sizes[i];
            }
        } else if (strcmp(term->content.str->stryng->str, "larger") == 0) {
            /* Find the standard size equal to or larger than the current size */
            int i = 6;

            while (i >= 0 && font_sizes[i] > size_points)
                i--;
            
            if (i < 0) /* original size smaller than any standard size */
                i = 0;
            
            /* Go one larger than that, if possible */
            if (i < 6)
                i++;

            size_points = font_sizes[i];
        } else {
            return FALSE;
        }

        *size = size_points * (resolution / 72.);
        return TRUE;
        
    } else if (term->type == TERM_NUMBER && term->content.num->type == NUM_PERCENTAGE) {
        *size *= term->content.num->val;
    } else if (get_length_from_term(style, term, TRUE, size) == VALUE_FOUND) {
        /* Convert from pixels to Pango units */
        *size *= 1024;
        return TRUE;
    }
    
    return FALSE;
}

static gboolean
font_weight_from_term(CRTerm      *term,
                      PangoWeight *weight,
                      gboolean    *weight_absolute)
{
    if (term->type == TERM_NUMBER) {
        int weight_int;
        
        /* The spec only allows numeric weights from 100-900, though Pango
         * will handle any number. We just let anything through.
         */
        if (term->content.num->type != NUM_GENERIC)
            return FALSE;

        weight_int = (int)(term->content.num->val + 0.5);

        *weight = weight_int;
        *weight_absolute = TRUE;
                
    } else if (term->type == TERM_IDENT) {
        /* FIXME: handle INHERIT */
        
        if (strcmp(term->content.str->stryng->str, "bold") == 0) {
            *weight = PANGO_WEIGHT_BOLD;
            *weight_absolute = TRUE;
        } else if (strcmp(term->content.str->stryng->str, "normal") == 0) {
            *weight = PANGO_WEIGHT_NORMAL;
            *weight_absolute = TRUE;
        } else if (strcmp(term->content.str->stryng->str, "bolder") == 0) {
            *weight = PANGO_WEIGHT_BOLD;
            *weight_absolute = FALSE;
        } else if (strcmp(term->content.str->stryng->str, "lighter") == 0) {
            *weight = PANGO_WEIGHT_LIGHT;
            *weight_absolute = FALSE;
        } else {
            return FALSE;
        }

    } else {
        return FALSE;
    }

    return TRUE;
}

static gboolean
font_style_from_term(CRTerm     *term,
                     PangoStyle *style)
{
    if (term->type != TERM_IDENT)
        return FALSE;

    /* FIXME: handle INHERIT */

    if (strcmp(term->content.str->stryng->str, "normal") == 0)
        *style = PANGO_STYLE_NORMAL;
    else if (strcmp(term->content.str->stryng->str, "oblique") == 0)
        *style = PANGO_STYLE_OBLIQUE;
    else if (strcmp(term->content.str->stryng->str, "italic") == 0)
        *style = PANGO_STYLE_ITALIC;
    else
        return FALSE;

    return TRUE;
}

static gboolean
font_variant_from_term(CRTerm       *term,
                       PangoVariant *variant)
{
    if (term->type != TERM_IDENT)
        return FALSE;

    /* FIXME: handle INHERIT */

    if (strcmp(term->content.str->stryng->str, "normal") == 0)
        *variant = PANGO_VARIANT_NORMAL;
    else if (strcmp(term->content.str->stryng->str, "small-caps") == 0)
        *variant = PANGO_VARIANT_SMALL_CAPS;
    else
        return FALSE;

    return TRUE;
}

PangoFontDescription *
hippo_canvas_style_get_font (HippoCanvasStyle *style)
{
    PangoStyle font_style;
    gboolean font_style_set = FALSE;
    PangoVariant variant;
    gboolean variant_set = FALSE;
    PangoWeight weight;
    gboolean weight_absolute;
    gboolean weight_set = FALSE;
    double parent_size;
    double size = 0.; /* Suppress warning */
    gboolean size_set = FALSE;
    char *family = NULL;
    int i;

    if (style->font_desc)
        return style->font_desc;
    
    style->font_desc = pango_font_description_copy(get_parent_font(style));
    parent_size = pango_font_description_get_size(style->font_desc);
    if (!pango_font_description_get_size_is_absolute(style->font_desc)) {
        double resolution = hippo_canvas_context_get_resolution(style->context);
        parent_size *= (resolution / 72.);
    }

    ensure_properties(style);

    for (i = 0; i < style->n_properties; i++) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, "font") == 0) {
            PangoStyle tmp_style = PANGO_STYLE_NORMAL;
            PangoVariant tmp_variant = PANGO_VARIANT_NORMAL;
            PangoWeight tmp_weight = PANGO_WEIGHT_NORMAL;
            gboolean tmp_weight_absolute = FALSE;
            double tmp_size;
            CRTerm *term = decl->value;

            /* A font specification starts with style/variant/weight
             * in any order. Each is allowed to be specified only once,
             * but we don't enforce that.
             */
            for (; term; term = term->next) {
                if (font_style_from_term(term, &tmp_style))
                    ;
                else if (font_variant_from_term(term, &tmp_variant))
                    ;
                else if (font_weight_from_term(term, &tmp_weight, &tmp_weight_absolute))
                    ;
                else
                    break;
            }

            /* The size is mandatory */

            if (term == NULL || term->type != TERM_NUMBER) {
                g_warning("Size missing from font property");
                continue;
            }

            tmp_size = parent_size;
            if (!font_size_from_term(style, term, &tmp_size)) {
                g_warning("Couldn't parse size in font property");
                continue;
            }
            
            if (term != NULL && term->type && TERM_NUMBER && term->the_operator == DIVIDE) {
                /* Ignore line-height specification */
                term = term->next;
            }
            
            /* the font family is mandatory - it is a comma-separated list of
             * names.
             */
            if (!font_family_from_terms(term, &family)) {
                g_warning("Couldn't parse family in font property");
                continue;
            }

            font_style = tmp_style;
            font_style_set = TRUE;
            weight = tmp_weight;
            weight_absolute = tmp_weight_absolute;
            weight_set = TRUE;
            variant = tmp_variant;
            variant_set = TRUE;
            
            size = tmp_size;
            size_set = TRUE;

        } else if (strcmp(decl->property->stryng->str, "family") == 0) {
            if (!font_family_from_terms(decl->value, &family)) {
                g_warning("Couldn't parse family in font property");
                continue;
            }
        } else if (strcmp(decl->property->stryng->str, "font-weight") == 0) {
            if (decl->value == NULL || decl->value->next != NULL)
                continue;

            if (font_weight_from_term(decl->value, &weight, &weight_absolute))
                weight_set = TRUE;
        } else if (strcmp(decl->property->stryng->str, "font-style") == 0) {
            if (decl->value == NULL || decl->value->next != NULL)
                continue;

            if (font_style_from_term(decl->value, &font_style))
                font_style_set = TRUE;
        } else if (strcmp(decl->property->stryng->str, "font-variant") == 0) {
            if (decl->value == NULL || decl->value->next != NULL)
                continue;

            if (font_variant_from_term(decl->value, &variant))
                variant_set = TRUE;
        } else if (strcmp(decl->property->stryng->str, "font-size") == 0) {
            gdouble tmp_size;
            if (decl->value == NULL || decl->value->next != NULL)
                continue;

            tmp_size = parent_size;
            if (font_size_from_term(style, decl->value, &tmp_size)) {
                size = tmp_size;
                size_set = TRUE;
            }
        } 
    }

    if (family)
        pango_font_description_set_family(style->font_desc, family);

    if (size_set)
        pango_font_description_set_absolute_size(style->font_desc, size);

    if (weight_set) {
        if (!weight_absolute) {
            /* bolder/lighter are supposed to switch between available styles, but with
             * font substitution, that gets to be a pretty fuzzy concept. So we use
             * a fixed step of 200. (The spec says 100, but that might not take us from
             * normal to bold.
             */
            
            PangoWeight old_weight = pango_font_description_get_weight(style->font_desc);
            if (weight == PANGO_WEIGHT_BOLD)
                weight = old_weight + 200;
            else
                weight = old_weight - 200;

            if (weight < 100)
                weight = 100;
            if (weight > 900)
                weight = 900;
        }
        
        pango_font_description_set_weight(style->font_desc, weight);
    }
    
    if (font_style_set)
        pango_font_description_set_style(style->font_desc, font_style);
    if (variant_set)
        pango_font_description_set_variant(style->font_desc, variant);

    return style->font_desc;
}

HippoCanvasThemeImage *
hippo_canvas_style_get_background_theme_image (HippoCanvasStyle *style)
{
    int i;

    if (style->background_theme_image_computed)
        return style->background_theme_image;

    style->background_theme_image = NULL;
    style->background_theme_image_computed = TRUE;

    ensure_properties(style);

    for (i = style->n_properties - 1; i >= 0; i--) {
        CRDeclaration *decl = style->properties[i];
        
        if (strcmp(decl->property->stryng->str, "-hippo-background-image") == 0) {
            CRTerm *term = decl->value;
            int lengths[4];
            int n_lengths = 0;
            int i;

            const char *url;
            int border_top;
            int border_right;
            int border_bottom;
            int border_left;

            char *filename;
            GError *error = NULL;
        
            /* First term must be the URL to the image */
            if (term->type != TERM_URI)
                goto next_property;

            url = term->content.str->stryng->str;
            
            term = term->next;

            /* Followed by 0 to 4 lengths */
            for (i = 0; i < 4; i++) {
                double value;

                if (term == NULL)
                    break;

                if (get_length_from_term(style, term, FALSE, &value) != VALUE_FOUND)
                    goto next_property;

                lengths[n_lengths] = (int)(0.5 + value);
                n_lengths++;

                term = term->next;
            }

            switch (n_lengths) {
            case 0:
                border_top = border_right = border_bottom = border_left = 0;
                break; 
            case 1:
                border_top = border_right = border_bottom = border_left = lengths[0];
                break; 
            case 2:
                border_top = border_bottom = lengths[0];
                border_left = border_right = lengths[1];
                break; 
            case 3:
                border_top = lengths[0];
                border_left = border_right = lengths[1];
                border_bottom = lengths[2];
                break; 
            case 4:
            default:
                border_top = lengths[0];
                border_right = lengths[1];
                border_bottom = lengths[2];
                border_left = lengths[3];
                break; 
            }

            filename = _hippo_canvas_theme_resolve_url(style->theme, decl->parent_statement->parent_sheet, url);
            if (filename == NULL)
                goto next_property;
            
            style->background_theme_image = hippo_canvas_theme_image_new(filename,
                                                                         border_top, border_right, border_bottom, border_left,
                                                                         &error);

            g_free(filename);

            if (style->background_theme_image == NULL) {
                g_warning("Failed to load theme image: %s", error->message);
                g_error_free(error);
                goto next_property;
            } else {
                return style->background_theme_image;
            }
        }

    next_property:
        ;
    }

    return NULL;
}

gboolean
hippo_canvas_style_paint (HippoCanvasStyle       *style,
                          cairo_t                *cr,
                          const char             *name,
                          double                  x,
                          double                  y,
                          double                  width,
                          double                  height)
{
    HippoCanvasThemeEngine *theme_engine;
    
    g_return_val_if_fail(HIPPO_IS_CANVAS_STYLE(style), FALSE);

    if (!style->theme)
        return FALSE;

    theme_engine = hippo_canvas_theme_get_theme_engine(style->theme);
    if (theme_engine == NULL)
        return FALSE;

    return hippo_canvas_theme_engine_paint(theme_engine,
                                           style, cr, name,
                                           x, y, width, height);
}
