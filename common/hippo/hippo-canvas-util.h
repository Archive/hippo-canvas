/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_UTIL_H__
#define __HIPPO_CANVAS_UTIL_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* utility methods used in hippo canvas */

#define HIPPO_TYPE_CAIRO_SURFACE           (hippo_cairo_surface_get_type ())
GType              hippo_cairo_surface_get_type (void) G_GNUC_CONST;

/* A boxed type for GdkRegion until one gets into gtk+ itself. */
#ifdef GDK_TYPE_REGION
#define PYGDK_TYPE_REGION  GDK_TYPE_REGION 
#else
GType pygdk_region_get_type (void) G_GNUC_CONST;

#define PYGDK_TYPE_REGION (pygdk_region_get_type ())
#endif /* GDK_TYPE_REGION */

G_END_DECLS

#endif /* __HIPPO_CANVAS_UTIL_H__ */
