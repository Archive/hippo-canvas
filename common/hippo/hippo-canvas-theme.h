/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_THEME_H__
#define __HIPPO_CANVAS_THEME_H__

#include <hippo/hippo-canvas-context.h>
#include <hippo/hippo-canvas-box.h>
#include <hippo/hippo-canvas-theme-engine.h>

G_BEGIN_DECLS

#define HIPPO_TYPE_CANVAS_THEME              (hippo_canvas_theme_get_type ())
#define HIPPO_CANVAS_THEME(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_CANVAS_THEME, HippoCanvasTheme))
#define HIPPO_CANVAS_THEME_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_CANVAS_THEME, HippoCanvasThemeClass))
#define HIPPO_IS_CANVAS_THEME(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_CANVAS_THEME))
#define HIPPO_IS_CANVAS_THEME_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_CANVAS_THEME))
#define HIPPO_CANVAS_THEME_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HIPPO_TYPE_CANVAS_THEME, HippoCanvasThemeClass))

GType             hippo_canvas_theme_get_type          (void) G_GNUC_CONST;

HippoCanvasTheme *hippo_canvas_theme_new (HippoCanvasThemeEngine *theme_engine,
                                          const char             *application_stylesheet,
                                          const char             *theme_stylesheet,
                                          const char             *default_stylesheet);

HippoCanvasThemeEngine *hippo_canvas_theme_get_theme_engine (HippoCanvasTheme *theme);


G_END_DECLS

#endif /* __HIPPO_CANVAS_THEME_H__ */
