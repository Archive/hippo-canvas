/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */

#include "hippo-animation-manager.h"
#include "hippo-canvas-marshal.h"

#define EVENT(animation, id) ((AnimationEvent *)g_ptr_array_index((animation)->events, (id)))

enum {
    AFTER_FRAME,
    LAST_SIGNAL
};

static int signals[LAST_SIGNAL];

struct _HippoAnimationManager {
    GObject parent;

    GPtrArray *animations;
    GArray *animation_start;

    double last_frame_time;
    guint frame_serial;
    guint frame_timeout;
    
    guint frame_pending : 1;
};

struct _HippoAnimationManagerClass {
    GObjectClass parent_clas;

    void (*after_frame) (HippoAnimationManager *animation,
                         guint                  frame_serial);
};
 
static void manager_remove_animation(HippoAnimationManager *manager,
                                     int                    index);

G_DEFINE_TYPE(HippoAnimationManager, hippo_animation_manager, G_TYPE_OBJECT)

#define ANIMATION(manager, i) ((HippoAnimation *)g_ptr_array_index((manager)->animations, (i)))
#define ANIMATION_START(manager, i) (g_array_index((manager)->animation_start, double, (i)))

static double
current_time(void)
{
    GTimeVal tv;

    g_get_current_time(&tv);

    return (double)tv.tv_sec + ((double)tv.tv_usec) / 1000000.;
}

static void
hippo_animation_manager_finalize(GObject *object)
{
    HippoAnimationManager *manager = HIPPO_ANIMATION_MANAGER(object);
    guint i;

    for (i = 0; i < manager->animations->len; i++)
        g_object_unref(ANIMATION(manager, i));

    g_ptr_array_free(manager->animations, TRUE);
    g_array_free(manager->animation_start, TRUE);
    
    G_OBJECT_CLASS(hippo_animation_manager_parent_class)->finalize(object);
}

static void
hippo_animation_manager_init(HippoAnimationManager *manager)
{
    manager->animations = g_ptr_array_new();
    manager->animation_start = g_array_new(FALSE, FALSE, sizeof(double));
}

static void
hippo_animation_manager_class_init(HippoAnimationManagerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = hippo_animation_manager_finalize;
    
    /**
     * HippoAnimationManager::after-frame
     *
     * Signal emitted after advancing to a new frame of the animation
     */
    signals[AFTER_FRAME] =
        g_signal_new ("after-frame",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET(HippoAnimationManagerClass, after_frame),
                      NULL, NULL,
                      hippo_canvas_marshal_VOID__UINT,
                      G_TYPE_NONE, 1, G_TYPE_UINT);
}

static void
animation_manager_do_frame(HippoAnimationManager *manager)
{
    guint i;
    
    manager->last_frame_time = current_time();
    manager->frame_pending = TRUE;

    for (i = 0; i < manager->animations->len; i++) {
        HippoAnimation *animation = ANIMATION(manager, i);
        hippo_animation_advance(animation, manager->last_frame_time - ANIMATION_START(manager, i));
    }
        
    manager->frame_serial++;

    g_signal_emit(manager, signals[AFTER_FRAME], 0, manager->frame_serial);
}

static gboolean
animation_manager_frame_timeout(gpointer data)
{
    HippoAnimationManager *manager = data;

    manager->frame_timeout = 0;
    animation_manager_do_frame(manager);

    return FALSE;
}

static void
animation_manager_update(HippoAnimationManager *manager)
{
    double now;
    double min_next_time = -1.;
    guint i;
    
    if (manager->frame_pending)
        return;
        
    now = current_time();

    i = 0;
    while (i < manager->animations->len) {
        HippoAnimation *animation = ANIMATION(manager, i);
        double start = ANIMATION_START(manager, i);
        double next_pos = hippo_animation_get_next_event_position(animation);
        
        if (next_pos < 0.) {
            manager_remove_animation(manager,i);
        } else {
            if (i == 0)
                min_next_time = start + next_pos;
            else
                min_next_time = MAX(min_next_time, start + next_pos);

            i++;
        }
    }

    if (i > 0) { /* At least one animation still activate */
        double next_frame_time = manager->last_frame_time + 1/30.;
        double next_time = MAX(min_next_time, next_frame_time);
        
        if (manager->frame_timeout) {
            g_source_remove(manager->frame_timeout);
            manager->frame_timeout = 0;
        }
        
        /*        g_print("%.5f\t%.5f\n", now - manager->last_frame_time, next_time - now); */

        if (next_time <= now) {
            animation_manager_do_frame(manager);
        } else {
            manager->frame_timeout = g_timeout_add((int)(0.5 + 1000 * (next_time - now)),
                                                   animation_manager_frame_timeout, manager);
        }
    }
}

static void
on_animation_cancel(HippoAnimation        *animation,
                    HippoAnimationManager *manager)
{
    guint i;
    
    for (i = 0; i < manager->animations->len; i++) {
        if (animation == ANIMATION(manager, i))
            manager_remove_animation(manager, i);
    }
}

static void
manager_remove_animation(HippoAnimationManager *manager,
                         int                    index)
{
    HippoAnimation *animation = ANIMATION(manager, index);
    
    g_ptr_array_remove_index(manager->animations, index);
    g_array_remove_index(manager->animation_start, index);

    g_signal_handlers_disconnect_by_func(animation,
                                         (gpointer)on_animation_cancel,
                                         manager);
    
    g_object_unref(animation);
}

HippoAnimationManager *
hippo_animation_manager_new (void)
{
    return g_object_new(HIPPO_TYPE_ANIMATION_MANAGER, NULL);
}

/**
 * hippo_animation_manager_add_animation:
 * @manager: the animation manager
 * @animation: the animation to add
 * @delay: time from the current time at which to start the animation, in seconds
 *
 * Add an animation to the animation manager. The animation starts at 
 * 'delay' seconds fromthe current time.
 */
void
hippo_animation_manager_add_animation (HippoAnimationManager *manager,
                                       HippoAnimation        *animation,
                                       double                 delay)
{
    double start_time;
    
    g_return_if_fail(HIPPO_IS_ANIMATION_MANAGER(manager));

    start_time = current_time() + delay;

    g_object_ref(animation);
    g_ptr_array_add(manager->animations, animation);
    g_array_append_val(manager->animation_start, start_time);

    g_signal_connect(animation, "cancel",
                     G_CALLBACK(on_animation_cancel), manager);

    animation_manager_update(manager);
}

/**
 * hippo_animation_manager_frame_complete:
 * @manager: the animation manager
 * @frame_serial: frame serial from the ::after-frame signal
 *
 * When the canvas container sees an ::after-frame signal from the animation
 * manager it should wait for all resizing and drawing in the canvas to be
 * complete (using whatever toolkit specific mechanisms are appropriate), and
 * then call this function.
 */
void
hippo_animation_manager_frame_complete (HippoAnimationManager *manager,
                                        guint                  frame_serial)
{
    g_return_if_fail(HIPPO_IS_ANIMATION_MANAGER(manager));

    manager->frame_pending = FALSE;
    
    animation_manager_update(manager);
}


