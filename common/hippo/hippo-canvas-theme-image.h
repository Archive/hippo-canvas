/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_THEME_IMAGE_H__
#define __HIPPO_CANVAS_THEME_IMAGE_H__

#include <glib-object.h>
#include <cairo.h>

G_BEGIN_DECLS

/* A HippoCanvasThemeImage encapsulates an image with specified unscaled borders
 * on each edge. This class has the secondary purpose of encapsulating the use
 * of librvsg to render SVG images without revealing it in the public API.
 */
#define HIPPO_TYPE_CANVAS_THEME_IMAGE              (hippo_canvas_theme_image_get_type ())
#define HIPPO_CANVAS_THEME_IMAGE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_CANVAS_THEME_IMAGE, HippoCanvasThemeImage))
#define HIPPO_CANVAS_THEME_IMAGE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_CANVAS_THEME_IMAGE, HippoCanvasThemeImageClass))
#define HIPPO_IS_CANVAS_THEME_IMAGE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_CANVAS_THEME_IMAGE))
#define HIPPO_IS_CANVAS_THEME_IMAGE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_CANVAS_THEME_IMAGE))
#define HIPPO_CANVAS_THEME_IMAGE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HIPPO_TYPE_CANVAS_THEME_IMAGE, HippoCanvasThemeImageClass))

typedef struct _HippoCanvasThemeImage HippoCanvasThemeImage;
typedef struct _HippoCanvasThemeImageClass HippoCanvasThemeImageClass;

#define HIPPO_CANVAS_THEME_IMAGE_ERROR (hippo_canvas_theme_image_error_quark())

enum {
    HIPPO_CANVAS_THEME_IMAGE_ERROR_FAILED
} HippoCanvasThemeImageError;

GType             hippo_canvas_theme_image_get_type          (void) G_GNUC_CONST;

GQuark            hippo_canvas_theme_image_error_quark (void);

HippoCanvasThemeImage *hippo_canvas_theme_image_new    (const char             *filename,
                                                        int                     border_top,
                                                        int                     border_right,
                                                        int                     border_bottom,
                                                        int                     border_left,
                                                        GError                **error);
void                   hippo_canvas_theme_image_render (HippoCanvasThemeImage  *image,
                                                        cairo_t                *cr,
                                                        int                     x,
                                                        int                     y,
                                                        int                     width,
                                                        int                     height);

G_END_DECLS

#endif /* __HIPPO_CANVAS_THEME_IMAGE_H__ */
