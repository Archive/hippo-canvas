/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_STYLE_H__
#define __HIPPO_CANVAS_STYLE_H__

#include "hippo-canvas-context.h"
#include "hippo-canvas-theme-image.h"

G_BEGIN_DECLS

#define HIPPO_TYPE_CANVAS_STYLE              (hippo_canvas_style_get_type ())
#define HIPPO_CANVAS_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HIPPO_TYPE_CANVAS_STYLE, HippoCanvasStyle))
#define HIPPO_CANVAS_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HIPPO_TYPE_CANVAS_STYLE, HippoCanvasStyleClass))
#define HIPPO_IS_CANVAS_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HIPPO_TYPE_CANVAS_STYLE))
#define HIPPO_IS_CANVAS_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HIPPO_TYPE_CANVAS_STYLE))
#define HIPPO_CANVAS_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HIPPO_TYPE_CANVAS_STYLE, HippoCanvasStyleClass))

typedef enum {
    HIPPO_CANVAS_LINK_NONE,
    HIPPO_CANVAS_LINK_LINK,
    HIPPO_CANVAS_LINK_VISITED
} HippoCanvasLinkType;

typedef enum {
    HIPPO_CANVAS_SIDE_LEFT,
    HIPPO_CANVAS_SIDE_RIGHT,
    HIPPO_CANVAS_SIDE_TOP,
    HIPPO_CANVAS_SIDE_BOTTOM
} HippoCanvasSide;

/* These are the CSS values; that doesn't mean we have to implement blink... */
typedef enum {
    HIPPO_TEXT_DECORATION_UNDERLINE    = 1 << 0,
    HIPPO_TEXT_DECORATION_OVERLINE     = 1 << 1,
    HIPPO_TEXT_DECORATION_LINE_THROUGH = 1 << 2,
    HIPPO_TEXT_DECORATION_BLINK        = 1 << 3
} HippoTextDecoration;

GType             hippo_canvas_style_get_type          (void) G_GNUC_CONST;

/* An element_type of G_TYPE_NONE means this style was created for the canvas root
 * element and matches a selector element name of 'canvas'
 */
HippoCanvasStyle *hippo_canvas_style_new (HippoCanvasContext    *context,
                                          HippoCanvasStyle      *parent_style,  /* can be null */
                                          HippoCanvasTheme      *theme,         /* can be null */
                                          GType                  element_type,
                                          const char            *element_id,
                                          const char            *element_class);

void                hippo_canvas_style_set_link_type (HippoCanvasStyle    *style,
                                                      HippoCanvasLinkType  link_type);
HippoCanvasLinkType hippo_canvas_style_get_link_type (HippoCanvasStyle    *style);

HippoCanvasStyle *hippo_canvas_style_get_parent (HippoCanvasStyle *style);

HippoCanvasTheme *hippo_canvas_style_get_theme (HippoCanvasStyle *style);

GType       hippo_canvas_style_get_element_type  (HippoCanvasStyle *style);
const char *hippo_canvas_style_get_element_id    (HippoCanvasStyle *style);
const char *hippo_canvas_style_get_element_class (HippoCanvasStyle *style);

/* Generic getters ... these are not cached so are less efficient. The other
 * reason for adding the more specific version is that we can handle the
 * details of the actual CSS rules, which can be complicated, especially
 * for fonts
 */
gboolean hippo_canvas_style_get_color (HippoCanvasStyle     *style,
                                       const char           *property_name,
                                       gboolean              inherit,
                                       guint32              *color);

gboolean hippo_canvas_style_get_double (HippoCanvasStyle     *style,
                                        const char           *property_name,
                                        gboolean              inherit,
                                        double               *value);

/* The length here is already resolved to pixels
 */
gboolean hippo_canvas_style_get_length (HippoCanvasStyle     *style,
                                        const char           *property_name,
                                        gboolean              inherit,
                                        gdouble              *length);

/* Specific getters for particular properties: cached
 */
guint32 hippo_canvas_style_get_background_color (HippoCanvasStyle *style);
guint32 hippo_canvas_style_get_foreground_color (HippoCanvasStyle *style);

double hippo_canvas_style_get_border_width (HippoCanvasStyle *style,
                                            HippoCanvasSide   side);
guint32 hippo_canvas_style_get_border_color(HippoCanvasStyle *style,
                                            HippoCanvasSide   side);
double hippo_canvas_style_get_padding      (HippoCanvasStyle *style,
                                            HippoCanvasSide   side);

HippoTextDecoration hippo_canvas_style_get_text_decoration (HippoCanvasStyle *style);

/* Font rule processing is pretty complicated, so we just hardcode it
 * under the standard font/font-family/font-size/etc names. This means
 * you can't have multiple separate styled fonts for a single item,
 * but that should be OK.
 */
PangoFontDescription *hippo_canvas_style_get_font (HippoCanvasStyle *style);

/* This is the getter for -hippo-background-image, which is different from
 * background-image in having provisions for unscaled borders.
 */
HippoCanvasThemeImage *hippo_canvas_style_get_background_theme_image (HippoCanvasStyle *style);

gboolean hippo_canvas_style_paint (HippoCanvasStyle       *style,
                                   cairo_t                *cr,
                                   const char             *name,
                                   double                  x,
                                   double                  y,
                                   double                  width,
                                   double                  height);

G_END_DECLS

#endif /* __HIPPO_CANVAS_STYLE_H__ */
