/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#ifndef __HIPPO_CANVAS_THEME_INTERNAL_H__
#define __HIPPO_CANVAS_THEME_INTERNAL_H__

#ifdef G_OS_WIN32
/* The libcroco/ directory doesn't exist uninstalled, which is how we build on windows */
#include <libcroco.h>
#else
#include <libcroco/libcroco.h>
#endif
#include "hippo-canvas-theme.h"

G_BEGIN_DECLS

void _hippo_canvas_theme_get_matched_properties (HippoCanvasTheme *theme,
                                                 HippoCanvasStyle *style,
                                                 CRDeclaration  ***properties,
                                                 int              *n_properties);

/* Resolve an URL from the stylesheet to a filename */
char *_hippo_canvas_theme_resolve_url (HippoCanvasTheme *theme,
                                       CRStyleSheet     *base_stylesheet,
                                       const char       *url);

G_END_DECLS

#endif /* __HIPPO_CANVAS_THEME_INTERNAL_H__ */
